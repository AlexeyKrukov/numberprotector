package krukov.alexey.my.algorithm;

import java.math.BigInteger;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Algorithm algo = new Algorithm();

        if(args[0].equals("-e")) {
            //System.out.println("Шифрование");
            //System.out.println("Введите число: ");
            //int number = scanner.nextInt();
            //System.out.println(algo.enCrypt(number));
            System.out.println("Зашифровано: " + algo.enCrypt(args[1]));

        } else if (args[0].equals("-d")) {
            //System.out.println("Дешифрование");
            System.out.println("Расшифровано: " + algo.deCrypt(args[1]));


        } else System.out.println("Неправильный аргумент");
    }
}
