package krukov.alexey.my.algorithm;

import java.math.BigInteger;

class Algorithm {

    private final char one = '1';

    String enCrypt(String number) {
        int count = 0;
        BigInteger bigInteger = new BigInteger(number);
        String strNumber = bigInteger.toString(2);
        for(int i = 0; i < strNumber.length(); i++) {
            if(strNumber.charAt(i) == one) {
                ++count;
            }
        }
        //Если сумма единиц четная
        if((count % 2) == 0) {
            strNumber = "11" + strNumber;
            //Образцовый алгоритм
            bigInteger = new BigInteger(strNumber, 2);
            strNumber = bigInteger.toString(8);
            strNumber += "17";
            bigInteger = new BigInteger(strNumber, 8);
            strNumber = bigInteger.toString(16);
            strNumber += "a";
        }

        //Если сумма единиц нечетная
        else {
            strNumber += "01";
            //Образцовый алгоритм
            bigInteger = new BigInteger(strNumber, 2);
            strNumber = bigInteger.toString(8);
            strNumber = "52" + strNumber;
            bigInteger = new BigInteger(strNumber, 8);
            strNumber = bigInteger.toString(16);
            strNumber = "d" + strNumber;
        }
        return strNumber;
    }

    String deCrypt(String number) {
        int count = 0;
        BigInteger bigInteger;

        if(number.charAt(0) == 'd') {
            number = number.substring(1);
            bigInteger = new BigInteger(number, 16);
            number = bigInteger.toString(8);
            //Вырезать первые 2 элемента
            number = number.substring(3);
            bigInteger = new BigInteger(number, 8);
            number = bigInteger.toString(2);
            //Вырезать последние 2 элемента
            number = number.substring(0);
            bigInteger = new BigInteger(number, 2);
            number = bigInteger.toString(10);
        } else if(number.charAt(number.length()) == 'a') {

        }
        return number;
    }
}
